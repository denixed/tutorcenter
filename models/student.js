var mongoose = require('../libs/mongoose');
var crypto = require('crypto');
var Schema = mongoose.Schema;
var ObjectID= Schema.Types.ObjectId;
var Group= require("./group");

var schema = new Schema({
  name: {
    type: String,
    required: true
  },
  email:{
    type:String,
    default:""
  },
  secname: {
    type: String,
    default:""
  },
  surname: {
    type: String,
    required: true

  },
  hashedPassword: {
    type: String,
    required: false
  },
  salt: {
    type: String,
    required: false
  },
  created: {
    type: Date,
    default: Date.now
  },
  tel: {
    type: String,
    default:""
  },
  img:{
    type:String,
  },
  date:{
    type:Date,
    default:null
  },
  form:{
    type:Number,
    required:true
  }
});

schema.index({ '$**': 'text'});

schema.methods.encryptPassword = function(password) {
  return crypto.createHmac('sha256', this.salt).update(password).digest('hex');
};
schema.post('save', function(st) {
  Group.find({students:st._id},function(err,gg){
    if(err) return console.error(err);
    gg.forEach(function(g){
      var up=false;
      up=false;
      g.cache.students.forEach(function(stl,i){
        if(String(stl._id)==String(st._id)) {
          g.cache.students[i]=st;
          up=true;
        }
      });
      if(up){
        g.markModified('cache');
        g.save(function(err){
          if(err) return console.error(err);
        });
      }
    });
  });
});
schema.virtual('password')
  .set(function(password) {
    this.salt = Math.random() + '';
    this.hashedPassword = this.encryptPassword(password);
  })
  .get(function() { return null; });

schema.methods.checkPassword = function(password) {
  return this.encryptPassword(password) === this.hashedPassword;
};


module.exports = mongoose.model('student', schema);
