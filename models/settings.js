var mongoose = require('../libs/mongoose');
var Schema = mongoose.Schema;
var ObjectID= Schema.Types.ObjectId;

var schema = new Schema({
  branchs:[{
    name: {
      type: String,
      default:''
    },
    shortname: {
      type: String,
      default:''
    }
  }],
  fines:[{
    typeF:{
      required:true,
      type:Number
    },
    message:{
      required:true,
      type:String
    },
    sum:{
      type:Number,
      min:0
    }
  }],
  courses:[{
    name: {
      type: String,
      default:''
    },
    shortname: {
      type: String,
      default:''
    }
  }],
  formats:[{
    name: {
      type: String,
      default:''
    },
    shortname: {
      type: String,
      default:''
    },
    p1:Number,
    p2:Number,
    pstep:Number
  }],
  subjects:[{
    name: {
      type: String,
      default:''
    },
    shortname: {
      type: String,
      default:''
    }
  }]

});


module.exports = mongoose.model('setting', schema);
