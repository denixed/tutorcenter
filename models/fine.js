var mongoose = require('../libs/mongoose');
var Schema = mongoose.Schema;
var ObjectID= Schema.Types.ObjectId;
var schema = new Schema({
  tutor: {
    type: ObjectID,
  },
  message:{
    type:String
  },
  comment:{
    type:String,
    default:''
  },
  date:{
    type:Date
  },
  sum:{
    type:Number,
    min:0
  },
  created: {
    type: Date,
    default: Date.now
  }
});


module.exports = mongoose.model('fine', schema);
