var mongoose = require('../libs/mongoose');
var Schema = mongoose.Schema;
var ObjectID= Schema.Types.ObjectId;

var schema = new Schema({
  name: {
    type: String,
    default:''
  },
  tutor: {
    type: ObjectID,
    required: true
  },
  group:{
    type:ObjectID,
    required:true
  },
  date:{
    type:Date,
    required:true
  },
  cache:{
    group:{
      comment:String,
      students:[ {
        type: ObjectID
      }],
      cache:{
        students:[{
          _id:ObjectID,
          form:Number,
          surname:String,
          secname:String,
          name:String
        }],
        tutor:{
          email:String,
          surname:String,
          secname:String,
          name:String,
          tel:String,
          prices:Object
        }
      },
      branch:{
        type:String,
        required:true
      },
      course:{
        type:String,
        required:true,
      },
      format:{
        type:String
      },
      subject:{
        type:String,
        required:true
      },
      dayW:{
        type:Number,
        min:0,
        max:6,
        required:true
      },
      time:{
        type:String,
        required:true
      },
      prices:{
        p1:{
          type:Number,
          default:-1,
          min:-1
        },
        p2:{
          type:Number,
          default:-1,
          min:-1
        },
        pstep:{
          type:Number,
          default:-1,
          min:-1
        }
      },
      newStudents:Schema.Types.Mixed,
      completed:{
        type:Boolean,
        defaut:false
      },
      completedCheck:{
        type:Boolean,
        defaut:false
      }
    },
    tutor:{
      email:String,
      surname:String,
      secname:String,
      name:String,
      tel:String
    }
  },
  students:[ObjectID],
  prices:{
    p1:{
      type:Number,
      default:-1,
      min:-1
    },
    p2:{
      type:Number,
      default:-1,
      min:-1
    },
    pstep:{
      type:Number,
      default:-1,
      min:-1
    }
  },
  checked:{
    type:String,
    default:""
  },
  presence:{
    type:Object,
    default:{}
  },
  marks:{
    type:Object,
    default:{}
  },
  theme:{
    type:String,
    default:''
  },
  last:{
    type:Boolean,
    default:false
  },
  payed:{
    type:Boolean,
    default:false
  },
  depositPayed:Boolean,
  fine:{
    message:String,
    sum:{
      type:Number,
      min:0
    },
    comment:String
  }
  
});

schema.index({ '$**': 'text'});

module.exports = mongoose.model('lesson', schema);
