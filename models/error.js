var mongoose = require('../libs/mongoose');
var Schema = mongoose.Schema;
var ObjectID= Schema.Types.ObjectId;
var schema = new Schema({
  user: {
    type: Object,
  },
  created: {
    type: Date,
    default: Date.now
  },
  error:{
    type:Object
  }
});


module.exports = mongoose.model('error', schema);
