var mongoose = require('../libs/mongoose');
var crypto = require('crypto');
var Schema = mongoose.Schema;
var ObjectID= Schema.Types.ObjectId;
var Lesson= require("./lesson");
var Group= require("./group");

var schema = new Schema({
  type:{
	  type: Number,
	  default:0
  },
  name: {
    type: String,
    required: true
  },
  email:{
    type:String,
    required:true,
    unique:true
  },
  secname: {
    type: String,
    required: true
  },
  surname: {
    type: String,
    required: true
  },
  hashedPassword: {
    type: String,
    required: true
  },
  salt: {
    type: String,
    required: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  tel: {
    type: String,
    required: true
  },
  students:[ObjectID],
  hidden_students:[ObjectID],
  prices:{
    type:Object,
    default:{}
  },
  tokenReset:String,
  filials:[{
	  type:String
  }]
});

schema.index({ '$**': 'text'});

schema.methods.encryptPassword = function(password) {
  return crypto.createHmac('sha256', this.salt).update(password).digest('hex');
};

schema.virtual('password')
  .set(function(password) {
    this.salt = Math.random() + '';
    this.hashedPassword = this.encryptPassword(password);
  })
  .get(function() { return null; });

schema.methods.checkPassword = function(password) {
  return this.encryptPassword(password) === this.hashedPassword;
};

schema.pre('save',function(cb){
  var self=this;
  Group.find({tutor:self._id},function(err,groups){
    if(err) return console.log('ERR - ',JSON.stringify(err));
    groups.forEach(function(l){
      l.cache.tutor=self;
      l.save(function(err){
        if(err) return console.log('ERR - ',JSON.stringify(err));
      });
    });
  });
  Lesson.find({tutor:self._id},function(err,lessons){
    if(err) return console.log('ERR - ',JSON.stringify(err));
    lessons.forEach(function(l){
      l.cache.tutor=self;
      l.save(function(err){
        if(err) return console.log('ERR - ',JSON.stringify(err));
      });
    });
  });
  cb();
});
module.exports = mongoose.model('user', schema);
