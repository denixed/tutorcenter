var mongoose = require('../libs/mongoose');
var Schema = mongoose.Schema;
var ObjectID= Schema.Types.ObjectId;
var Lesson= require("./lesson");
var config=require("../libs/config");
var schema = new Schema({
  comment: {
    type: String,
    default:""
  },
  tutor: {
    type: ObjectID,
    required: true
  },
  students:[ {
    type: ObjectID
  }],
  created: {
    type: Date,
    default: Date.now
  },
  cache:{
    students:[{
      _id:ObjectID,
      form:Number,
      surname:String,
      secname:String,
      name:String
    }],
    tutor:{
      email:String,
      surname:String,
      secname:String,
      name:String,
      tel:String,
      prices:Object
    }
  },
  branch:{
    type:String,
    required:true
  },
  course:{
    type:String,
    required:true,
  },
  format:{
    type:String
  },
  subject:{
    type:String,
    required:true
  },
  dayW:{
    type:Number,
    min:0,
    max:6,
    required:true
  },
  time:{
    type:String,
    required:true
  },
  prices:{
    p1:{
      type:Number,
      default:-1,
      min:-1
    },
    p2:{
      type:Number,
      default:-1,
      min:-1
    },
    pstep:{
      type:Number,
      default:-1,
      min:-1
    }
  },
  completed:{
    type:Boolean,
    defaut:false
  },
  completedCheck:{
    type:Boolean,
    defaut:false
  },
  merging:{
    type:Object
  }
});
schema.post('save',function(cb){
  var self=this;
  Lesson.find({group:self._id},function(err,lessons){
    if(err) return console.log('ERR - ',JSON.stringify(err));
    lessons.forEach(function(l){
      if(self.merging) {
        l.group=self.merging._id;
        l.cache.group=self.merging;
      }else{
         l.cache.group=self;
      }
      l.save(function(err){
        if(err) return console.log('ERR - ',JSON.stringify(err));
      });
    });
   if(self.merging) self.remove();
  });
});
schema.pre('save',function(cb){
  if(config.scripts.autoformat) {
    this.format=config.scripts.autoformat(this);
  }
  cb();
});
schema.index({ '$**': 'text'});

module.exports = mongoose.model('group', schema);
