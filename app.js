var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var mongoStore=require('./libs/mongoStore');
var compression = require('compression');
var Lesson=require("./models/lesson");
var User=require("./models/user");
var config = require("./libs/config");
var mailer = require("./libs/mailer");

var app = express();
global.__prFolder=__dirname;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
if(!process.env.production){
  app.use(logger('dev'));
}
app.use(compression());
app.use(session({ path: '/', httpOnly: true, secret: config.session.secret, maxAge: 21600000, store:mongoStore}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req,res,next){
  res.locals.url=req.originalUrl;
  if(req.session.user){
        User.findOne({_id:req.session.user._id},function(err,user){
            if(err) return next (err);
            res.locals.user=req.session.user=user;
            if(user.type) res.locals.admin=user.type;
            next();
        });
    }else if(req.originalUrl!="/signin"&&req.originalUrl!="/signup"&&req.originalUrl!="/signin/"&&req.originalUrl!="/signup/"&req.path!="/restore_password"&&req.path!="/restore_password/") res.redirect('/signin/');
    else next();
}
);
app.use(function(req,res,next){
  if(req.session.user){
		if(!(req.session.user.type&&req.session.user.type==123321||req.session.user.type==240901)) return next();
        d={"checked":""};
		if(req.session.user.type==123321) d['cache.group.branch']={
			$in: req.session.user.filials
		};
		Lesson.count(d).exec(function(err,n){
          if(err) return next(err);
          res.locals.counterL=n;
          next();
        });
    }else next();
});
require("./routes")(app);

app.use(function(req, res, next) {
  var err = new Error('Не найдено');
  err.status = 404;
  err.stack="";
  next(err);
});

app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = process.env.production ? {}: err;
  res.status(err.status || 500);
  if(err.status!=404&&err.status!=406&&err.status!=403){
	var text= req.originalUrl + '\n'+ JSON.stringify(err) + '\n'+ JSON.stringify(req.session.user);
	mailer({to:'denixed@ya.ru','subject':'Ошибка на сервере',text:text,html:text.replace(/\n/g,'<br>')});
   
  }
  res.render('error');
});

module.exports = app;
