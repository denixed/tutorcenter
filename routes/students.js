var express = require('express');
var router = express.Router();
var Student = require("../models/student");

router.get('/', function(req, res, next) {
  res.render('students');
});
router.get('/list', function(req, res, next) {
	Array.prototype.diff = function(a) {
		return this.filter(function(i) {return a.indexOf(i) < 0;});
	};
  Student.find({_id:{ $in: req.session.user.students.diff(req.session.user.hidden_students)}}).select('_id surname name secname form').exec(function(err,users){
      if(err) return next(err);
      res.send(users);
  });
});
router.get('/listofvalues/:id', function(req, res, next) {
  var check={
    name:1,
    surname:1,
    secname:1
  };
  if(!check[req.params.id]) return next();
  Student.find().distinct(req.params.id,function(err,users){
      if(err) return next(err);
      res.send(users);
  });
});
/*router.get('/list/id/:id', function(req, res, next) {
  Student.findOne({_id:req.params.id},function(err,user){
      if(err) return next(err);
      res.send(user);
  });
});*/
router.post('/add',function(req,res,next){
    console.log(req.body);
  var student=new Student(req.body);
  student.save(function(err){
    if(err) return next(err);
    req.session.user.students.push(student._id);
    req.session.user.markModified('students');
    req.session.user.save(function(err){
      if(err) return next(err);
      res.send('ok');  
    });
  });
});
router.post('/addfromdb/',function(req,res,next){
	console.log(req.body);
  Student.findOne(req.body,function(err,s){
    if(err) return next(err);
	console.log(s);
	console.log(req.session.user);
    if(!s) return next();
    req.session.user.students.push(s._id);
    req.session.user.markModified('students');
    req.session.user.save(function(err){
      if(err) return next(err);
      res.send('ok');  
    });
  });
});
router.post('/update',function(req,res,next){
  var json=JSON.parse(req.body.json);
  if(!req.session.user.students.find(function(st){
    return json._id==st;
  })){
      var err= new Error();
      err.status=403;
      err.message="Вы не преподаёте у данного ученика";
      err.stack='';
      return next(err);
  }
  Student.findOne({_id:json._id},function(err,student){
    if(err) return next(err);
    if(!student) {
      err= new Error();
      err.status=404;
      err.message="Ученик не найден";
      err.stack='';
      return next(err);
    }
    var key;
    for (key in json) {
          student[key] = key in student ? json[key] : undefined;
    }
    student.save(function(err){
      if(err) return next(err);
      res.send('ok');
    });
  });
});

router.get('/hide/:id',function(req,res,next){
	//if(!req.session.user.hidden_students) req.session.user.hidden_students=[];
	req.session.user.hidden_students.push(req.params.id);
	req.session.user.markModified('hidden_students');
	req.session.user.save();
	res.send('ok');
});
router.get('/show/:id',function(req,res,next){
	for(var i=0;i<req.session.user.hidden_students.length;i++){
		if(String(req.session.user.hidden_students[i])==req.params.id){
			req.session.user.hidden_students.splice(i,1)
			i=req.session.user.hidden_students.length;
		}
	}
	req.session.user.markModified('hidden_students');
	req.session.user.save();
	res.send('ok');
});


module.exports = router;
