var express = require('express');
var router = express.Router();
var User = require('../models/user');
var mailer = require("../libs/mailer");
function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 15; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}
router.post('/',function(req,res,next){
  if(req.body.q){
   var t=JSON.parse(req.body.q);
   var q={
        _id:t._id||'',
        tel:t.tel||'',
        tokenReset:t.tokenReset||'0',
        email:t.email||''
   };
   User.findOne(q,function(err,t){
      if(err) return next(err);
      if(t){
        t.tokenReset='';
        t.password=req.body.password;
        t.save(function(err){
          if(err) return next(err);
          var text= 'Данные для входа: \n email: '+t.email+'\n password: '+req.body.password;
          mailer({to:t.email,'subject':'Пароль изменён',text:text,html:text.replace(/\n/g,'<br>')});
        });
      }
      res.redirect('/');
   });
   return; 
  }
  User.findOne({email:req.body.email,tel:req.body.tel},function(err,t){
    if(err) return next(err);
    if(t){
      t.tokenReset=makeid();
      var text= 'Для востановление аккаунта воспользуйтесь <a href=\'https://egevpare.tk/restore_password/?q=';
      text+=JSON.stringify({
        _id:t._id,
        tel:t.tel,
        tokenReset:t.tokenReset,
        email:t.email
      })+'\'>ссылкой</a>.';
      t.save();
      console.log(text);
      mailer({to:t.email,'subject':'Востановление доступа',text:'Для востановление аккаунта воспользуйтесь ссылкой',html:text});
    }
    res.render('error',{message:'Проверьте свою почту',error:{
      status:'Успешно',stack:''
    }});
  });
});
router.get('/',function(req,res,next){
    if(req.query.q) return res.render('restore_password_final');
    res.render('restore_password');
});
module.exports = router;
