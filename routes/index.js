var E=require("../models/error");
var config = require("../libs/config");

module.exports = function(app){
  app.use('/', require("./home"));
  app.use('/signin', require("./signin")); 
  app.use('/signout', require("./signout")); 
  app.use('/signup', require("./signup")); 
  app.use('/workspace', require("./workspace"));  
  app.use('/students', require("./students"));  
  app.use('/groups', require("./groups"));  
  app.use('/lessons', require("./lessons"));
  require("./admin")(app);
  app.use('/settings', require("./settings"));
  app.use('/timeline', require("./timeline"));
  app.use('/restore_password', require("./restore_password"));
  app.get('/feed.json',function(req,res,next){
	  if(req.session.user&&config.admins[req.session.user._id]){
		res.send('{"notifications": [{"bell": '+res.locals.counterL+'}],"refresh_time" : 60 }');
	  }else res.send('{}');
  })
  app.post('/error/log',function(req,res,next){
    res.sendStatus(200);
    var err;
    err=JSON.parse(req.body.json);
    err.user=req.session.user;
    console.log(err);
    (new E(err)).save(function(err){
      if(err) throw err;
    });
  });
};