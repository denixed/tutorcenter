var express = require('express');
var router = express.Router();

var settingsModel = require("../models/settings");

/* GET home page. */

router.get('/',function(req,res,next){
    settingsModel.findOne(function(err,settings){
       if(err) return next(err); 
       res.send(settings);
    });
});
module.exports = router;
