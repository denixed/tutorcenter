var express = require('express');
var router = express.Router();
var Lesson = require("../models/lesson");
var Group = require("../models/group");

router.get('/', function(req, res, next) {
  res.render('lessons');
});
router.get('/list', function(req, res, next) {
  var d=req.query;
  if(d.json) d=JSON.parse(d.json);
  d.tutor=req.session.user._id;
  Lesson.find(d).sort({date:-1}).exec(function(err,lessons){
      if(err) return next(err);
      res.send(lessons);
  });
});
router.post('/add',function(req,res,next){
  var lesson=new Lesson(JSON.parse(req.body.json));
  lesson.tutor=req.session.user._id;
  if(Date.now()-Number(new Date(lesson.date))<-1800000){
    var err= new Error();
    err.status=406;
    err.message="Не правильная дата";
    err.stack='';
    return next(err);
  } 
  lesson.cache.tutor=req.session.user;
  lesson.students=lesson.cache.group.students;
  lesson.checked='';
  var lst=lesson.last||false;
  Group.findOne({_id:lesson.group},function(err, g) {
      if(err) return next(err);
      if(!g) {
        var err= new Error();
        err.status=406;
        err.message="Группа не найдена";
        err.stack='';
        return next(err);
      }else{
       cb2(g);
      }
  });
  var self=lesson;
  var cb2=function(g){
    if(lst){
      g.completed=true;
      g.save(function(err){
        if(err) return next(err);
        cb3();
      });
    }
    else cb3();
  };
  var cb3=function(){
    Lesson.findOne({'group':self.group,'tutor':self.tutor,'first':true},function(err,l){
      if(err) return next(err);
      else if(!l){
        Lesson.find({'group':self.group,'tutor':self.tutor}).sort({date:1}).exec(function(err,ls){
          if(err) return next(err);
          if(ls.length){
            ls[0].first=true;
            ls[0].save(function(err){
              if(err) return next(err);
              cb4();
            }); 
          }else{
            self.first=true;
            cb4();
          }
        });
      }else if(l.date>self.date){
        l.first=false;
        l.save(function(err){
          if(err) return next(err);
          self.first=true;
          cb4();
        });
      }else {
        cb4();
      }
    });
  };
  var cb4=function(){
    self.save(function(err){
      if(err) return next(err);
      res.send('ok');
    });
  };
});
router.post('/update',function(req,res,next){
  var q=JSON.parse(req.body.json);
  Lesson.findOne({_id:q._id,tutor:req.session.user._id,checked:""},function(err,lesson){
    if(err) return next(err);
    if(!lesson) return next();
    var key;
    for (key in q) {
        lesson[key] = key in lesson ? q[key] : undefined;
    }
    if(lesson.checked) {
        err= new Error();
        err.status=403;
        err.message="Занятие уже подтверждено";
        err.stack='';
        return next(err);
    }
    if(Date.now()-Number(new Date(lesson.date))<-1800000){
        err= new Error();
        err.status=406;
        err.message="Не правильная дата";
        err.stack='';
        return next(err);
    } 
    lesson.save(function(err){
      if(err) return next(err);
      res.send('ok');
    });
  });
});
router.post('/remove',function(req,res,next){
  Lesson.findOne({_id:req.body._id,tutor:req.session.user._id,checked:""}).remove(function(err){
    if(err) return next(err);
    res.send('ok');
  });
});
module.exports = router;