var express = require('express');
var router = express.Router();
var mongoose =require(__prFolder+"/libs/mongoose");
var settingsModel = require(__prFolder+"/models/settings");
var User = require(__prFolder+"/models/user");
var Lesson = require(__prFolder+"/models/lesson");
var config = require(__prFolder+"/libs/config");
var http=require("http");

router.get('/settings',function(req,res,next){
    res.render("admin/settings");
});
router.get('/db',function(req,res,next){
    res.render("admin/db");
});
router.get('/list/:id',function(req,res,next){
	var q={};
	if(req.query.q){
		q=JSON.parse(req.query.q);
	}
    mongoose.model(req.params.id).find(q, function(err,d) {
        if(err) return next(err);
        res.send(d);
    });
});
router.get('/settings/list',function(req,res,next){
    settingsModel.findOne(function(err,settings){
       if(err) return next(err); 
       res.send(settings);
    });
});
router.post('/settings/update',function(req,res,next){
    settingsModel.findOne(function(err,settings){
       if(err) return next(err); 
       var settings1=JSON.parse(req.body.settings);
       console.log(settings1);
        var key;
        for (key in settings1) {
            settings[key] = key in settings ? settings1[key] : undefined;
        }
       settings.save(function(err){
           if(err) return next(err);
           res.send('ok');
       });
    });
});
router.get('/remunerations', function(req, res, next) {
  res.render('admin/remunerations',{count_rmn_for_lesson:config.scripts.count_rmn_for_lesson});
});
router.get('/remunerations/deposit/lessonslist/:id', function(req, res, next) {
  User.findOne({_id:req.params.id},function(err,u){
      if(err) return next(err);
      if(!u){
        err= new Error();
        err.status=404;
        err.message="Не найдено";
        err.stack='';
        return next(err);
      }
      var lessons=[];
      u.depositOperation.forEach(function(o){
          if(o.lesson) lessons.push(o.lesson);
      });
      Lesson.find({_id:{$in:lessons}}).exec(function(err,n){
          if(err) return next(err);
          res.send(n);
      });
      
  });
  
  
});
router.post('/remunerations/deposit/payout/', function(req, res, next) {
    User.findOne({'_id':req.body.user},function(err, t) {
        if(err) return next(err);
        if(!t){
            err= new Error();
            err.status=404;
            err.message="Не найдено";
            err.stack='';
            return next(err);
        }
        t.deposit-=Number(req.body.sum);
        t.depositOperation.push({sum:Number(req.body.sum)*(-1),user:req.session.user,typeO:1});
        t.markModified('depositOperation');
        console.log(t);
        t.save(function(err){
          if(err) return next(err);
          res.send('ok');
        });
    });
    
});
router.get('/timeline', function(req, res, next) {
  res.render('admin/timeline');
});

module.exports = router;