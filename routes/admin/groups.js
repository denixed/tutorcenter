var express = require('express');
var router = express.Router();
var Group = require(__prFolder+"/models/group");
var Lesson = require(__prFolder+"/models/lesson");
var async=require("async");
router.get('/',function(req,res,next){
    res.render("admin/groups");
});
router.get('/list', function(req, res, next) {
	req.session.user.filials.push('');
  var q=req.query;
  if(q.json) q=JSON.parse(q.json);
  if(req.session.user.type==123321) q['branch']={
		$in: req.session.user.filials
	};
  Group.find(q).exec(function(err,g){
    if(err) return next(err);
    res.send(g);
  });
});
router.post('/update',function(req,res,next){
  Group.findOne({_id:req.body._id},function(err,group){
    if(err) return next(err);
    if(!group) return next();
    var key;
    for (key in req.body) {
        if(key=="prices"){
          group.prices=JSON.parse(req.body.prices);
        }else{
          group[key] = key in group ? req.body[key] : undefined;
        }
    }
    group.save(function(err){
      if(err) return next(err);
      res.send('ok');
    });
  });
});
router.post('/remove',function(req,res,next){
  Group.findOne({_id:req.body._id}).remove(function(err,group){
    if(err) return next(err);
    res.send('ok');
  });
});
router.get('/complete', function(req, res, next) {
  var d=req.query.id;
  Group.findOne({_id:d},function(err,group){
      if(err) return next(err);
      if(!group){
        err = new Error('Группа не найдена');
        err.status = 404;
        err.stack="";
        return next(err);
      }
      Lesson.find({group:group._id,tutor:group.tutor}).sort({date:-1}).exec(function(err,ll){
        if(err) return next(err);
        if(!ll.length){
          err = new Error('В группе нет уроков.');
          err.status = 404;
          err.stack="";
          return next(err);
        }
        async.every(ll, function(l,cb){
          l.last=false;
          l.checked=req.session.user._id;
          l.save(cb);
        }, function(err, results) {
          if(err) return next(err);
          ll[0].last=true;
          ll[0].save(function(err){
            if(err) return next(err);
            group.completed=true;
            group.completedCheck=true;
            group.save(function(err){
              if(err) return next(err);
              res.send('ok');
            });
          });
        });
      });
  });
});
router.post('/merge',function(req,res,next){
  Group.findOne({_id:req.body._id1},function(err,g1){
    if(err) return next(err);
    if(!g1) return next();
    Group.findOne({_id:req.body._id2},function(err,g2){
      if(err) return next(err);
      if(!g2) return next();
      if(String(g1.tutor)!=String(g2.tutor)) {
        err = new Error('Преподователи групп различаются.');
        err.status = 403;
        err.stack="";
        return next(err);
      }
      var students=g1.cache.students;
      g2.cache.students.forEach(function(st){
        var tmp = true;
        for(var i=0;i<students.length&&tmp;i++){
          if(String(st._id)==String(students[i]._id)) tmp=false;
        }
        if(tmp){
          students.push(st);
        }
      });
      var st_ids=[];
      students.forEach(function(s){
        st_ids.push(s._id);
      });
      g1.cache.students=students;
      g1.students=students;
      g2.merging=g1;
      g1.save(function(err){
        if(err) return next(err);
        g2.save(function(err){
          if(err) return next(err);
          res.send('ok');

        });
      });
    });
    
  });
});
router.post('/update',function(req,res,next){
  Group.findOne({_id:req.body._id},function(err,group){
    if(err) return next(err);
    if(!group) return next();
    var key;
    for (key in req.body) {
        if(key=="prices"){
          group.prices=JSON.parse(req.body.prices);
        }else{
          group[key] = key in group ? req.body[key] : undefined;
        }
    }
    group.save(function(err){
      if(err) return next(err);
      res.send('ok');
    });
  });
});
module.exports = router;