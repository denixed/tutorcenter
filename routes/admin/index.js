var config=require(__prFolder+"/libs/config");
var check=function(req, res, next) {
  if(req.session.user.type==123321||req.session.user.type==240901){
      next();
  }else{
    var err = new Error('Нет доступа');
    err.status = 403;
    err.stack="";
    next(err);    
  }
};
var checkSU=function(req, res, next) {
  if(req.session.user.type==240901){
      next();
  }else{
    var err = new Error('Нет доступа');
    err.status = 403;
    err.stack="";
    next(err);    
  }
};
module.exports = function(app){
  app.use('/admin/',check, require("../workspace"));
  app.use('/admin/groups',check, require("./groups")); 
  app.use('/admin/lessons',check, require("./lessons")); 
  app.use('/admin/students',check, require("./students")); 
  app.use('/admin/tutors',check, require("./tutors"));  
  app.use('/admin/login',check, require("./login"));  
  app.use('/admin/stats',check, require("./stats"));  
  app.use('/admin/controll',checkSU, require("./controll"));  
  app.use('/admin/',check, require("./other"));  
};