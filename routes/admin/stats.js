var express = require('express');
var router = express.Router();
var Lesson = require('../../models/lesson');
var async = require('async');

router.get('/lessons/year',function(req,res,next){
	var year = (new Date()).getUTCFullYear()
	async.map([
		{ $gt: year+'-01-01', $lt: year+'-02-01' },
		{ $gt: year+'-02-01', $lt: year+'-03-01' },
		{ $gt: year+'-03-01', $lt: year+'-04-01' },
		{ $gt: year+'-04-01', $lt: year+'-05-01' },
		{ $gt: year+'-05-01', $lt: year+'-06-01' },
		{ $gt: year+'-06-01', $lt: year+'-07-01' },
		{ $gt: year+'-07-01', $lt: year+'-08-01' },
		{ $gt: year+'-08-01', $lt: year+'-09-01' },
		{ $gt: year+'-09-01', $lt: year+'-10-01' },
		{ $gt: year+'-10-01', $lt: year+'-11-01' },
		{ $gt: year+'-11-01', $lt: year+'-12-01' },
		{ $gt: year+'-11-01', $lt: (year+1)+'-01-01' }
	],function(d,cb){
		Lesson.count({
			date:d
		},function(err,c){
			if(err) cb(err);
			cb(null,c);
		})
	},function(err,data){
		if(err) return next(err);
		res.send(data);
	})
});
router.get('/lessons/month',function(req,res,next){
	var year = (new Date()).getUTCFullYear(),year2;
	var month = (new Date()).getUTCMonth();
	var month1=month+1;
	if(month1<10) month1='0'+month1;
	var month2=(month+1)%12+1;
	if(month2<10) month2='0'+month2;
	if(month==11) year2=year+1;
	else year2=year;
	Lesson.count({
			date:{ $gt: year+'-'+month1+'-01', $lt: year2+'-'+month2+'-01' }
		},function(err,c){
			if(err) next(err);
			res.send(200,c)
		})
})
router.get('/lessons/',function(req,res,next){
	Lesson.count(function(err,c){
		if(err) next(err);
		res.send(200,c)
	})
})


module.exports = router;
