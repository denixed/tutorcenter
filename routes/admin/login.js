var express = require('express');
var router = express.Router();
var User = require('../../models/user');

router.post('/',function(req,res,next){
  var us=req.body.id||"";
  User.findOne({_id:us},function(err,user){
    if(err) return next(err);
    if(!user) us=0;
    else {
      req.session.user=res.locals.user=user;
    }
    if(us){
      res.redirect("/workspace/");
    }
    else{
      res.redirect("/admin/login/#!");
    }
  });
});
router.get('/',function(req,res,next){
    res.render('admin/login');
});
module.exports = router;
