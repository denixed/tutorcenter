var express = require('express');
var router = express.Router();
var Lesson = require(__prFolder+"/models/lesson");
var Tutor = require(__prFolder+"/models/user");
var Settings = require(__prFolder+"/models/settings")
var config = require(__prFolder+"/libs/config")
var async = require("async");
router.get('/', function(req, res, next) {
  res.render('admin/lessons');
});
router.get('/list', function(req, res, next) {
  var q={};
  if(req.query.min&&req.query.max) q={'date':{ $gt: req.query.min, $lt: req.query.max }};
  else if(req.query.json) q=JSON.parse(req.query.json);
  else q=req.query;
  if(req.session.user.type==123321) q['cache.group.branch']={
	$in: req.session.user.filials
};
Lesson.find(q,{score: {$meta: "textScore"}}).sort({date:-1,score:{$meta:"textScore"}}).limit(1000).exec(function(err,lessons){
    if(err) return next(err);
    res.send(lessons);
  });
});
router.get('/list/step/:step', function(req, res, next) {
	q={};
	if(req.session.user.type==123321) q['cache.group.branch']={
		$in: req.session.user.filials
	};
	if(req.params.step=='count'){
		Lesson.count(q,function(err,n){
			if(err) return next(err);
			res.send(200,parseInt(n/300)+1)
		})
		return;
	}
	Lesson.find(q).sort({date:-1}).skip((req.params.step-1)*300).limit(300).exec(function(err,lessons){
		if(err) return next(err);
		res.send(lessons);
	});
});

router.post('/update',function(req,res,next){
  var q=JSON.parse(req.body.json);
  Lesson.findOne({_id:q._id},function(err,lesson){
    if(err) return next(err);
    if(!lesson) return next();
    var key;
    for (key in q) {
      lesson[key] = key in lesson ? q[key] : undefined;
    }
    if(Date.now()-Number(new Date(lesson.date))<-1800000){
        err= new Error();
        err.status=406;
        err.message="Не правильная дата";
        err.stack='';
        return next(err);
    } 
    lesson.save(function(err){
      if(err) return next(err);
      res.send('ok');
    });
  });
});
router.post('/remove',function(req,res,next){
  Lesson.findOne({_id:req.body._id}).exec(function(err, l) {
      if(err) return next(err);
      l.remove(function(err){
        if(err) return next(err);
        res.send('ok');
      });
  });
});
router.get('/check/:id', function(req, res, next) {
  Lesson.findOne({_id:req.params.id},function(err,l){
    if(err) return next(err);
    if(!l) {
      var err = new Error('Не найдено');
      err.status = 404;
      err.stack="";
      return next(err);
    }
    if(!l.cache.group.format){
      var err = new Error('Формат не установлен');
      err.status = 406;
      err.stack="";
      return next(err);
    }
    if(l.checked!='') return res.send(200);
    l.checked=req.session.user._id;
    l.save(function(err){
        if(err) return next(err);
        res.send('OK');
    });
        
  });
});
router.get('/uncheck/:id', function(req, res, next) {
  Lesson.findOne({_id:req.params.id},function(err,l){
    if(err) return next(err);
    if(!l) {
      err = new Error('Не найдено');
      err.status = 404;
      err.stack="";
      return next(err);
    }
    l.checked='';
    l.save(function(err){
        if(err) return next(err);
        res.send('OK');
    });
        
  });
});
router.post('/payout/',function(req,res,next){
  if(!req.body.json) {
    var err = new Error('Не правильный формат');
    err.status = 406;
    err.stack="";
    return next(err);
  }
  try{
    var q=JSON.parse(req.body.json);
  }catch(e){
    var err = new Error('Не правильный формат');
    err.status = 406;
    err.stack="";
    return next(err);
  }
  var i=0,ii=0;
  if(q.payed.length){
    ii++;
    Lesson.find({_id:{"$in":q.payed}},function(err,ll){
      console.log(ll);
      if(err) return next(err);
      if(!ll.length){
        var err = new Error('Не найдены занятия');
        err.status = 406;
        err.stack="";
        return next(err);
      }
      async.each(ll, function(l,cb){
        console.log(l._id);
        l.payed=true;
        l.save(cb);
      }, function(err) {
        if(err) return next(err);
        cbn();
      });
    });
  }
  if(q.unpayed.length){
    ii++;
    Lesson.find({_id:{"$in":q.unpayed}},function(err,ll){
      console.log(ll);
      if(err) return next(err);
      if(!ll.length){
        var err = new Error('Не найдены занятия');
        err.status = 406;
        err.stack="";
        return next(err);
      }
      async.each(ll, function(l,cb){
        console.log(l._id);
        l.payed=false;
        l.save(cb);
      }, function(err) {
        if(err) return next(err);
        cbn();
      });
    });
  }
  if(q.payeddeposit.length){
    ii++;
    Lesson.find({_id:{"$in":q.payeddeposit}},function(err,ll){
      console.log(ll);
      if(err) return next(err);
      if(!ll.length){
        var err = new Error('Не найдены занятия');
        err.status = 406;
        err.stack="";
        return next(err);
      }
      async.each(ll, function(l,cb){
        console.log(l._id);
        l.depositPayed=true;
        l.save(cb);
      }, function(err) {
        if(err) return next(err);
        cbn();
      });
    });
  }
  if(q.unpayeddeposit.length){
    ii++;
    Lesson.find({_id:{"$in":q.unpayeddeposit}},function(err,ll){
      console.log(ll);
      if(err) return next(err);
      if(!ll.length){
        var err = new Error('Не найдены занятия');
        err.status = 406;
        err.stack="";
        return next(err);
      }
      async.each(ll, function(l,cb){
        console.log(l._id);
        l.depositPayed=false;
        l.save(cb);
      }, function(err) {
        if(err) return next(err);
        cbn();
      });
    });
  }
  if(ii==0) cbn();
  var cbn=function(){
    i++;
    if(i>=ii) res.send('ok');
  };
});
module.exports=router;