var express = require('express');
var router = express.Router();
var Tutor = require(__prFolder+"/models/user");
var Student = require(__prFolder+"/models/student");
var mailer = require(__prFolder+"/libs/mailer");


router.get('/', function(req, res, next) {
  res.render('admin/controll');
});

router.post('/setAdmin',function(req,res,next){
  Tutor.findOne({_id:req.body._id},function(err,tutor){
    if(err) return next(err);
    if(!tutor) return next();
    tutor.type=123321;
    tutor.save(function(err){
      if(err) return next(err);
      res.send('ok');
    });
  });
});
router.post('/setSU',function(req,res,next){
  Tutor.findOne({_id:req.body._id},function(err,tutor){
    if(err) return next(err);
    if(!tutor) return next();
    tutor.type=240901;
    tutor.save(function(err){
      if(err) return next(err);
      res.send('ok');
    });
  });
});
router.post('/setTutor',function(req,res,next){
  Tutor.findOne({_id:req.body._id},function(err,tutor){
    if(err) return next(err);
    if(!tutor) return next();
    tutor.type=0;
    tutor.save(function(err){
      if(err) return next(err);
      res.send('ok');
    });
  });
});
router.post('/setFilials',function(req,res,next){
  Tutor.findOne({_id:req.body._id},function(err,tutor){
    if(err) return next(err);
    if(!tutor) return next();
    tutor.filials=JSON.parse(req.body.filials);
    tutor.save(function(err){
      if(err) return next(err);
      res.send('ok');
    });
  });
});


module.exports=router;