var express = require('express');
var router = express.Router();
var Student = require(__prFolder+"/models/student");

router.get('/', function(req, res, next) {
  res.render('admin/students');
});
router.get('/list', function(req, res, next) {
  Student.find(function(err,users){
      if(err) return next(err);
      res.send(users);
  });
});
router.post('/add',function(req,res,next){
  var student=new Student(req.body);
  student.save(function(err){
    if(err) return next(err);
    res.send('ok');
  });
});
router.post('/update',function(req,res,next){
  var json=JSON.parse(req.body.json);
  Student.findOne({_id:json._id},function(err,student){
    if(err) return next(err);
    if(!student) {
      err= new Error();
      err.status=404;
      err.message="Ученик не найден";
      err.stack='';
      return next(err);
    }
    var key;
    for (key in json) {
          student[key] = key in student ? json[key] : undefined;
    }
    student.save(function(err){
      if(err) return next(err);
      res.send('ok');
    });
  });
});

module.exports=router;