var express = require('express');
var router = express.Router();
var Tutor = require(__prFolder+"/models/user");
var Student = require(__prFolder+"/models/student");
var mailer = require(__prFolder+"/libs/mailer");


router.get('/', function(req, res, next) {
  res.render('admin/tutors');
});

router.post('/update',function(req,res,next){
  Tutor.findOne({_id:req.body._id},function(err,tutor){
    if(err) return next(err);
    if(!tutor) return next();
    var key;
    for (key in req.body) {
        if(key=='prices') tutor.prices=JSON.parse(req.body.prices);
        else  tutor[key] = key in tutor ? req.body[key] : undefined;
    }
    tutor.save(function(err){
      if(err) return next(err);
      res.send('ok');
    });
  });
});
router.post('/add',function(req,res,next){
  var tutor=new Tutor(req.body);
  tutor.save(function(err){
    if(err) return next(err);
    var text= 'Данные для входа: \n email: '+tutor.email+'\n password: '+req.body.password;
    mailer({to:tutor.email,'subject':'Данные для входа',text:text,html:text.replace(/\n/g,'<br>')});
    res.send('ok');
  });
});
router.post('/remove',function(req,res,next){
  Tutor.findOne({_id:req.body._id}).remove(function(err){
    if(err) return next(err);
    res.send('ok');
  });
});
router.get('/students', function(req, res, next) {
   Student.find({_id:{ $in: JSON.parse(req.query.students)}},function(err,users){
      if(err) return next(err);
      res.send(users);
  }); 
});

module.exports=router;