var express = require('express');
var async = require('async');
var router = express.Router();
var Group = require("../models/group");
var Student = require("../models/student");

router.get('/', function(req, res, next) {
  res.render('groups');
});
router.get('/list', function(req, res, next) {
  var d=req.query;
  d.tutor=req.session.user._id;
  d.completedCheck={$ne:true};
  Group.find(d,function(err,groups){
      if(err) return next(err);
      res.send(groups);
  });
});
router.post('/add',function(req,res,next){
    console.log(req.body);
  var group=new Group(req.body);
  group.tutor=req.session.user._id;
  group.students=JSON.parse(req.body.students);
  group.cache.tutor=req.session.user;
  group.format=undefined;
  async.each(group.students,function(s,cb){
    Student.findOne({_id:s},function(err,s){
      if(err) cb(err);
      if(!s){ 
        cb('Ученик не существует')
      }
      else{ 
        group.cache.students.push(s);
         group.markModified('cache');
      }
	  cb(null)
    });
  }, function(err) {
	if(err) return next(err);
	group.save(function(err){
		if(err) return next(err);
		res.send('ok');
	});
  });
  
});
router.post('/update',function(req,res,next){
  Group.findOne({_id:req.body._id,tutor:req.session.user._id},function(err,group){
    if(err) return next(err);
    if(!group) return next();
    var key;
    for (key in req.body) {
        group[key] = key in group ? req.body[key] : undefined;
    }
    group.format=undefined;
    group.save(function(err){
      if(err) return next(err);
      res.send('ok');
    });
  });
});
/*router.post('/remove',function(req,res,next){
  Group.findOne({_id:req.body._id,tutor:req.session.user._id}).remove(function(err,group){
    if(err) return next(err);
    res.send('ok');
  });
  
  
});*/
router.post('/addstudent',function(req,res,next){
  Group.findOne({_id:req.body._id,tutor:req.session.user._id}).exec(function(err,group){
    if(err) return next(err);
    var st=req.body.student;
    if(!st){
      err= new Error();
      err.status=406;
      err.message="Ученик не указан";
      err.stack='';
      return next(err);
    }
    if(!group) return next();
    if(group.students.find(function(e){ 
      return e==st;
      
    })) {
      return res.send('ok');
    }
    group.students.push(st);
    group.markModified('students');
    Student.findOne({_id:st},function(err,s){
      if(err) return next(err);
      if(!s){ 
        return next();
      }
      else{ 
        group.cache.students.push(s);
        group.markModified('cache');
      }
      group.save(function(err){
        if(err) return next(err);
        res.send('ok');
      });
    });
  });
});
module.exports = router;
