var l=require("./models/lesson");
var g=require("./models/group");
var i=0;
l.find({students:[null]}).exec(function(err,ll){
    if(err) return console.log(err);
    
    ll.forEach(function(l){
        g.findOne(l.group,function(err,g){
            if(err) return console.log(err);
            l.students=g.students;
            l.markModified('students');
            l.save(function(err){
                console.log(err||i++);
            });
        })

    });
})