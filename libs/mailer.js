var nodemailer = require('nodemailer');
var config =require("./config");
var transporter = nodemailer.createTransport(config.mail);
transporter.verify(function(error, success) {
   if (error) {
        console.log(error);
   } else {
        console.log('Server is ready to take our messages');
   }
});

var mailOptions = config.mailOptions;
module.exports=function(data,cb){
    data.from=mailOptions.from;
    transporter.sendMail(data, function(err) {
        if (err) {
            return cb(err);
        }
        cb();
    });
};
